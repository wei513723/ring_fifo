#!/bin/bash

startTimeStamp=0
endTimeStamp=0

testfun(){
    loopn=$1
    loopvar=0

    while (( $loopn > $loopvar ))
    do
        echo "start the $loopvar test..."
        ./build/ring_fifo
        cmp tmpfile/read_file tmpfile/write_file
        if [ $? -ne 0 ]
        then
            return 1
        fi
        let "loopvar++"
    done

    return 0
}

if [ $# -lt 1 ] || [ $# -gt 1 ]
then
    echo "Usage $0 <times>"
else
    make

    current=`date "+%Y-%m-%d %H:%M:%S"`
    timeStamp=`date -d "$current" +%s`
    #将current转换为时间戳，精确到毫秒
    startTimeStamp=$((timeStamp*1000+`date "+%N"`/1000000))
    echo "startTimeStamp: $startTimeStamp"

    testfun $1
    if [ $? -eq 0 ]
    then
        echo ">>>all test ok!!!"
    else
        echo ">>>some error occur!!!"
    fi

    current=`date "+%Y-%m-%d %H:%M:%S"`
    timeStamp=`date -d "$current" +%s`
    #将current转换为时间戳，精确到毫秒
    endTimeStamp=$((timeStamp*1000+`date "+%N"`/1000000))
    echo "endTimeStamp: $endTimeStamp"

    val=`expr $endTimeStamp - $startTimeStamp`
    echo "TimeStampDiff: $val"
fi
